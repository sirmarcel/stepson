# `stepson` -- `son` for trajectories

This is the draft of a new `trajectory.son` parsing infrastructure for `FHI-vibes`.

`vibes` is therefore a requirement.

[![look at this!!](https://asciinema.org/a/MsSdps9XuBhbFCag1XB8nBIaY.svg)](https://asciinema.org/a/MsSdps9XuBhbFCag1XB8nBIaY)

## Todo

### Writing

- [ ] Add a trajectory writer

### Testing/Verification

- [ ] Check that `attrs` is somehwat correct

### Edge cases/functionality

- [ ] Prefer supercell for displacements
- [ ] Warn if cell is not constant for displacements
- [ ] Add `aims_uuid`
- [ ] Make `cell` and `masses` and `volume` optional (in the sense of changing the `getters` to default to reference values automatically, and having the option to not save those values)
- [ ] Reintroduce `displacements`
- [ ] Add `energies` as property
- [ ] Add iterator over `Atoms`
- [ ] Deal with custom properties in a unified way

## Architecture

`engine` defines the abstract infrastructure for parsing: every entry of the `son` file is first translated into an intermediate representation called `Record`,  essentially just a `dict` of `ndarrays`. The `Reader` then combines all of these `ndarrays` for all the keys into arrays. This has the advantage of not requiring us to store the full dataset twice in memory, we only need to store each row twice, temporarily, as it is created from the individual arrays.

`trajectory` contains the concrete implementation for parsing `trajectory.son` files. The `Record` subclass for trajectories is called `Step`, hence the name!
