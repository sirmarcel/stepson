from pathlib import Path
import xarray as xr

def open_dataset(folder):
    folder = Path(folder)
    if folder.is_file():
        return xr.open_dataset(folder)
    else:
        return xr.open_mfdataset(f"{folder}/*.nc")
