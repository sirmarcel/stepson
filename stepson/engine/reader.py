import numpy as np
import son
import pathlib


from stepson import comms

from .record import Record

BATCH_SIZE = 1000000


class Reader:

    record = Record  # set in subclass

    def get_keys_and_defaults(self, prototype):
        # implemented in subclass
        raise NotImplementedError

    def post(self, result):
        # implemented in subclass
        return self.metadata, result

    def __init__(self, file, batch_size=BATCH_SIZE):
        self.file = pathlib.Path(file)
        assert self.file.is_file(), f"{self.file} does not exist"

        self.batch_size = batch_size

    def __call__(self):
        self.reporter = comms.reporter()
        comms.announce(f"reading {self.file} in batches of {self.batch_size}")
        self.batch = 0

        self.keys_and_defaults()

        _, reader = son.open(self.file)

        self.reporter.start(f"working on batch {self.batch}")
        for data in self.read(reader):
            data = self.arrays(data)
            data = self.post(data)
            self.reporter.done()
            yield data

            self.batch += 1
            self.reporter.start(f"working on batch {self.batch}")

        self.reporter.done()
        comms.announce(f"done reading {self.file}")

    def keys_and_defaults(self):
        self.metadata, reader = son.open(self.file)

        try:
            first = next(reader)
        except StopIteration:
            raise RuntimeError("cannot read empty file")

        self.keys, self.defaults = self.get_keys_and_defaults(first)

    def read(self, reader):
        data = {key: [] for key in self.keys}

        self.reporter.step("processing entries", spin=False)

        for i, entry in enumerate(reader):
            self.reporter.tick(f"#{i}")
            self.add_entry(data, entry)
            if i > 0 and i % self.batch_size == 0:
                yield data

                data = {key: [] for key in self.keys}
                self.reporter.step("processing entries", spin=False)

        if any([len(v) > 0 for v in data.values()]):
            yield data

    def add_entry(self, data, entry):
        dct = self.get_record(entry).data

        for key in self.keys:
            if key in dct:
                data[key].append(dct[key])
            else:
                if self.defaults[key] is not None:
                    data[key].append(self.defaults[key])
                else:
                    raise KeyError(f"{key} is required")

    def get_record(self, entry):
        return self.record.from_entry(entry)

    def arrays(self, data):
        self.reporter.step("making arrays")

        # re-assign inside of dict to free memory
        for key in self.keys:
            data[key] = np.array(data[key])

        return data
