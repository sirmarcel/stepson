class Record:

    meta = "metadata"  # set in subclass, name of metadata key
    required = []  # set in subclass, required keys
    optional = []  # set in subclass, optional keys

    def __init__(self, data, metadata):
        self.data = data  # should be dict-like
        self.metadata = metadata  # should be dict-like

    @classmethod
    def from_entry(cls, dct):
        # implemented in subclass
        raise NotImplementedError
