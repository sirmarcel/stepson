import numpy as np
import xarray as xr
from vibes import keys

from stepson import comms

from .helpers import split_by_chunks


def get_virials(dataset, verbose=False):

    if keys.stresses_potential in dataset:
        if verbose:
            comms.talk("computing heat flux using stresses")
        virials = dataset[keys.stresses_potential]
    elif keys.virials in dataset:
        if verbose:
            comms.talk("computing heat flux using virials")
        virials = dataset[keys.virials]
    else:
        if verbose:
            comms.talk(
                "both virials and stresses present, computing heat flux using virials"
            )
        virials = dataset[keys.virials]

    return virials


def get_mean_virials(virials):
    mean_virials = virials.dropna("time").mean(dim="time")
    return mean_virials.compute()


def get_heat_flux(dataset, mean_virials=None, verbose=True):
    from ase.units import fs

    virials = get_virials(dataset, verbose=verbose)
    velocities = dataset[keys.velocities] * fs * 1000

    heat_flux_full = (
        (virials * velocities.rename(a="b"))
        .sum(dim=("I"), skipna=False)
        .sum(dim=("b"), skipna=False)
    )
    heat_flux_full = heat_flux_full.compute()

    if mean_virials is None:
        mean_virials = get_mean_virials(virials)

    aux_virials = xr.ones_like(virials) * mean_virials

    heat_flux_aux = (
        (aux_virials * velocities.rename(a="b"))
        .sum(dim=("I"), skipna=False)
        .sum(dim=("b"), skipna=False)
    ).rename(keys.heat_flux_aux)
    heat_flux_aux = heat_flux_aux.compute()
    heat_flux_aux = heat_flux_aux.where(~np.isnan(heat_flux_full))

    heat_flux = (heat_flux_full - heat_flux_aux).rename(keys.heat_flux)

    return heat_flux, heat_flux_aux


def get_heat_flux_dataset(
    dataset, verbose=True, steps_for_frequency=15000, include_frequency=True
):
    from stepson.trajectory import add_property

    reporter = comms.reporter(silent=(not verbose))
    reporter.start("making heat flux dataset")

    reporter.step("compute volume and temperature")
    add_property(dataset, keys.temperature)
    add_property(dataset, keys.volume)

    volume = dataset[keys.volume].mean().compute().data
    temperature = dataset[keys.temperature].mean().compute().data

    heat_flux, heat_flux_aux = get_heat_flux_in_chunks(dataset, reporter, verbose=verbose)

    if include_frequency:
        reporter.step("compute characteristic frequency")
        frequency = get_frequency(dataset, maxsteps=steps_for_frequency, verbose=verbose)

    reporter.step("compute prefactor")
    prefactor = get_prefactor(volume, temperature)
    reporter.finish_step()

    if verbose:
        msg = f"V={volume:.2f}Å^3, T={temperature:0.2f}K"
        if include_frequency:
            msg += f", ω={frequency:.2f}THz"

        comms.talk(msg)
        comms.talk(f"prefactor={prefactor:.2f} W/mK / (eV/Å^3/fs)")

    attrs = {
        "volume": volume,
        "temperature": temperature,
        "prefactor": prefactor,
    }
    if include_frequency:
        attrs["frequency"] = frequency

    dataset = xr.Dataset(
        {keys.heat_flux: heat_flux, keys.heat_flux_aux: heat_flux_aux}, attrs=attrs
    )

    reporter.done()
    return dataset


def get_heat_flux_in_chunks(dataset, reporter, verbose=True):
    reporter.step("compute mean virials")

    virials = get_virials(dataset, verbose=verbose)
    mean_virials = get_mean_virials(virials)

    reporter.step("compute heat flux", spin=False)

    # for some strange reason, we have to do this by hand,
    # dask/xarray will run out of memory otherwise
    chunks = []
    for i, chunk in enumerate(split_by_chunks(dataset)):
        reporter.tick(f"chunk {i}")
        chunks.append(get_heat_flux(chunk, mean_virials=mean_virials, verbose=False))

    heat_flux, heat_flux_aux = zip(*chunks)

    heat_flux = xr.concat(heat_flux, dim="time")
    heat_flux_aux = xr.concat(heat_flux_aux, dim="time")

    reporter.finish_step()

    return heat_flux, heat_flux_aux


def get_frequency(dataset, maxsteps=15000, verbose=True):
    from vibes.green_kubo import get_lowest_vibrational_frequency

    if verbose:
        comms.talk(f"computing VDOS up to {maxsteps} steps")

    dataset = dataset.isel(time=slice(0, maxsteps))

    return get_lowest_vibrational_frequency(dataset[keys.velocities])


def get_prefactor(volume, temperature):
    from vibes.green_kubo import gk_prefactor

    return gk_prefactor(volume, temperature, verbose=False)
