import numpy as np
import xarray as xr

from vibes import keys, defaults, dimensions
from vibes.green_kubo import get_hf_data
from vibes.helpers.filter import get_filtered

from stepson import comms


def get_kappa_dataset(
    dataset,
    verbose=True,
    maxsteps=None,
    freq=None,
    aux=False,
    window_factor=defaults.window_factor,
):
    """Compute kappa from heat flux dataset

    YOLO copy from vibes, to be fixed later


    Workflow:
        1. get heat flux autocorrelation function (HFACF) and the integrated kappa
        2. get lowest significant vibrational frequency and get its time period
        3. filter integrated kappa with this period
        4. get HFACF by time derivative of this filtered kappa
        5. filter the HFACF with the same filter
        6. estimate cutoff time from the decay of the filtered HFACF

    (2) is done ahead of time to avoid costly computation

    """

    reporter = comms.reporter(silent=(not verbose))

    reporter.start("computing kappa")

    prefactor = dataset.attrs["prefactor"]

    if maxsteps is not None:
        reporter.step(f"truncate to {maxsteps}")
        dataset = dataset.isel(time=slice(0, maxsteps))

    reporter.step("get heat flux")
    heat_flux = dataset[keys.heat_flux]

    if aux:
        heat_flux += dataset[keys.heat_flux_aux]

    reporter.step("compute hfacf")
    hfacf, kappa = get_hf_data(heat_flux, distribute=False)

    hfacf *= prefactor
    kappa *= prefactor

    reporter.step("filter + cutoff")

    if freq is None:
        if "frequency" in dataset.attrs:
            freq = dataset.attrs["frequency"]
        else:
            comms.warn("ω not given, guessing 1 THz!")
            freq = 1.0

    if abs(freq) < 0.001:
        comms.warn(f"ω is {freq} THz, CHECK VDOS!", level=2)

    window_fs = window_factor / freq * 1000

    k_filtered = get_filtered(
        kappa, window_fs=window_fs, antisymmetric=True, verbose=False
    )
    k_gradient = kappa.copy()

    # compute derivative with j = dk/dt = dk/dn * dn/dt = dk/dn / dt
    dt = float(kappa.time[1] - kappa.time[0])
    k_gradient.data = np.gradient(k_filtered, axis=0) / dt

    j_filtered = get_filtered(k_gradient, window_fs=window_fs, verbose=False)

    # get cutoff times from j_filtered and save respective kappas
    ts = np.zeros([3, 3])
    ks = np.zeros([3, 3])

    for (ii, jj) in np.ndindex(3, 3):
        j = j_filtered[:, ii, jj]
        times = j.time[j < 0]
        if len(times) > 1:
            ta = times.min()
        else:
            comms.warn(f"no cutoff time found", level=1)
            ta = 0
        ks[ii, jj] = k_filtered[:, ii, jj].sel(time=ta)
        ts[ii, jj] = ta

    reporter.step("compile dataset")

    attrs = dataset.attrs.copy()
    attrs.update({"gk_window_fs": window_fs})

    _filtered = "_filtered"
    data = {
        keys.hf_acf: hfacf,
        keys.hf_acf + _filtered: j_filtered,
        keys.kappa_cumulative: kappa,
        keys.kappa_cumulative + _filtered: k_filtered,
        keys.kappa: (dimensions.tensor, ks),
        keys.time_cutoff: (dimensions.tensor, ts),
    }

    dataset = xr.Dataset(data, coords=kappa.coords, attrs=attrs)

    reporter.done()

    if verbose:
        comms.talk(f"Estimated filter window size")
        comms.talk(f" lowest vibrational frequency: {freq:.4f} THz")
        comms.talk(f" corresponding window size:    {window_fs:.4f} fs")
        comms.talk(f" window multiplicator used:    {window_factor:.4f} fs")

        k_diag = np.diag(ks)
        for m in ["Cutoff times (fs):", *np.array2string(ts, precision=3).split("\n")]:
            comms.talk(m)
        comms.talk(
            f"Kappa is:       {np.mean(k_diag):.3f} +/- {np.std(k_diag) / 3**.5:.3f}"
        )
        for m in ["Kappa^ab is: ", *np.array2string(ks, precision=3).split("\n")]:
            comms.talk(m)

    return dataset
