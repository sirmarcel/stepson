import xarray as xr
from vibes import keys, dimensions


keys_and_dimensions = {
    keys.positions: dimensions.time_atom_vec,
    keys.cell: dimensions.time_tensor,
    keys.velocities: dimensions.time_atom_vec,
    keys.forces: dimensions.time_atom_vec,
    keys.energy_potential: dimensions.time,
    keys.stress_potential: dimensions.time_tensor,
    keys.stresses_potential: dimensions.time_atom_tensor,
    keys.virials: dimensions.time_atom_tensor,
    "energies_potential": dimensions.time_atom,
    "heat_flux": dimensions.time_vec,
    "heat_flux_potential": dimensions.time_vec,
    "heat_flux_convective": dimensions.time_vec,
}


def get_properties(reporter, data, time):
    for key, dimension in keys_and_dimensions.items():
        get_property(reporter, data, time, key, dimension)


def get_property(reporter, data, time, key, dimension):
    if key in data:
        reporter.step(f"add {key}")
        data[key] = xr.DataArray(
            data[key],
            dims=dimension,
            coords=time,
            name=key,
        )
