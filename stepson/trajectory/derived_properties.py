import numpy as np
import xarray as xr
from vibes import keys, dimensions

getters = {}  # will be filled in by @getter


def getter(name, requires=[]):
    def decorator(function):
        def wrapper(dataset, ensure_required=True):
            if ensure_required:
                for required in requires:
                    add_property(dataset, required)

            dataarray = function(dataset)
            return dataarray.rename(name)

        getters[name] = wrapper

        return wrapper

    return decorator


def add_property(dataset, key):
    if key not in dataset:
        getter = getters[key]
        dataarray = getter(dataset)
        dataset.update({key: dataarray})


def reference_atoms(dataset):
    from vibes.helpers.converters import dict2atoms
    from json import loads

    return dict2atoms(loads(dataset.attrs[keys.reference_atoms]))


@getter(keys.cell)
def get_cell(dataset):
    return xr.DataArray(
        reference_atoms(dataset).get_cell().array, dims=dimensions.lattice
    ).expand_dims(dim={keys.time: dataset.time})


@getter(keys.volume, requires=[keys.cell])
def get_volume(dataset):
    cell = dataset[keys.cell]
    volume = np.sum(
        cell[:, 0, :] * np.cross(cell[:, 1, :], cell[:, 2, :]),
        axis=1,
    )

    return volume


@getter(keys.momenta)
def get_momenta(dataset):
    masses = xr.DataArray(dataset.attrs["masses"], dims=dimensions.I)
    momenta = dataset[keys.velocities] * masses  # sensitive to ordering

    return momenta


@getter(keys.energy_kinetic, requires=[keys.momenta])
def get_energy_kinetic(dataset):
    momenta = dataset[keys.momenta]
    velocities = dataset[keys.velocities]

    energy_kinetic = (0.5 * momenta * velocities).sum(dim=("I", "a"))

    return energy_kinetic


@getter(
    keys.stress_kinetic, requires=[keys.volume, keys.momenta, keys.stress_potential]
)
def get_stress_kinetic(dataset):
    invmasses = 1.0 / xr.DataArray(dataset.attrs["masses"], dims=dimensions.I)
    invvolume = 1.0 / dataset[keys.volume]
    momenta = dataset[keys.momenta]

    stress_kinetic = (
        -1.0 * (momenta * momenta.rename(a="b") * invmasses).sum(dim="I") * invvolume
    )

    stress_potential = dataset[keys.stress_potential]

    # make sure nans match
    stress_kinetic = stress_kinetic.where(~np.isnan(stress_potential))

    return stress_kinetic


@getter(keys.stress, requires=[keys.stress_kinetic, keys.stress_potential])
def get_stress(dataset):
    return dataset[keys.stress_kinetic] + dataset[keys.stress_potential]


@getter(keys.pressure, requires=[keys.stress])
def get_pressure(dataset):
    from vibes.helpers.xarray import xtrace

    return xtrace(dataset[keys.stress]).rename(keys.pressure) * (-1.0 / 3.0)


@getter(keys.pressure_kinetic, requires=[keys.stress_kinetic])
def get_pressure_kinetic(dataset):
    from vibes.helpers.xarray import xtrace

    return xtrace(dataset[keys.stress_kinetic]).rename(keys.pressure_kinetic) * (
        -1.0 / 3.0
    )


@getter(keys.pressure_potential, requires=[keys.stress_potential])
def get_pressure_potential(dataset):
    from vibes.helpers.xarray import xtrace

    return xtrace(dataset[keys.stress_potential]).rename(keys.pressure_potential) * (
        -1.0 / 3.0
    )


@getter(keys.temperature, requires=[keys.energy_kinetic, keys.forces])
def get_temperature(dataset):
    from ase.units import kB

    ekin = dataset[keys.energy_kinetic]
    dof = len(dataset[keys.forces][0]) * 3

    temperature = 2 * ekin / (dof * kB)

    return temperature


all_properties = getters
