import numpy as np
import xarray as xr

from vibes import keys, dimensions

from stepson.engine import Reader
from stepson import comms
from .step import Step
from .base_properties import get_properties
from .derived_properties import add_property, all_properties

nan = float("nan")
BATCH_SIZE = 25000


def reader(file, properties=[], full=False, batch_size=BATCH_SIZE):
    if full:
        properties = all_properties
    reader = TrajectoryReader(file, batch_size=batch_size, properties=properties)
    for chunk in reader():
        yield chunk


class TrajectoryReader(Reader):
    record = Step

    def __init__(self, file, batch_size=BATCH_SIZE, properties=[]):
        super().__init__(file, batch_size=batch_size)

        self.derived_properties = properties

    def get_keys_and_defaults(self, prototype):
        proto = Step.from_entry(prototype)
        data = proto.data
        keys = list(data.keys())

        defaults = {k: None for k in Step.required}
        for key in Step.optional:
            if key in keys:
                defaults[key] = nan * np.zeros_like(data[key])

        return keys, defaults

    def post(self, data):

        reference_atoms = get_reference_atoms(self.metadata)
        time = get_time(self.reporter, data, self.metadata)

        # convert values in data to DataArray
        get_properties(self.reporter, data, time)

        # re-assign data to make sure memory is freed
        data = xr.Dataset(data, coords=time)

        data = remove_duplicates(self.reporter, data)
        data.attrs = get_attrs(self.reporter, data, self.metadata, reference_atoms)

        for prop in self.derived_properties:
            self.reporter.step(f"add {prop}")
            add_property(data, prop)

        self.reporter.finish_step()  # avoids odd printing
        return data


def get_reference_atoms(metadata):
    from vibes.helpers.converters import dict2atoms

    if "supercell" in metadata:
        atoms_dict = metadata["supercell"]
    else:
        atoms_dict = metadata["atoms"]

    atoms = dict2atoms(atoms_dict)
    return atoms


def get_time(reporter, data, metadata):
    reporter.step("add time")
    time_unit = get_time_unit(reporter, metadata)
    dt = data.pop("dt")
    nsteps = data.pop("nsteps").astype(float)
    time = dt * nsteps / time_unit

    return {keys.time: time}


def get_time_unit(reporter, metadata):
    try:
        fs = metadata["MD"]["fs"]
    except KeyError:
        comms.warn("time unit not found in trajectory metadata, use ase.units.fs")
        from ase.units import fs

    return fs


def remove_duplicates(reporter, dataset):
    reporter.step("deduplication")
    unique_times, index = np.unique(dataset[keys.time], return_index=True)

    dn = len(dataset[keys.time]) - len(unique_times)

    if dn > 0:
        comms.warn(f"Drop {dn} duplicate timesteps. CHECK TRAJECTORY!", level=1)
        dataset = dataset.isel({keys.time: index})

    return dataset


def get_attrs(reporter, dataset, metadata, reference_atoms):
    reporter.step("set metadata")
    from vibes.structure.misc import get_sysname
    from vibes.fourier import get_timestep
    from vibes.helpers.converters import atoms2json, dict2json

    time = dataset.time

    attrs = {
        keys.name: keys.trajectory,
        keys.system_name: get_sysname(reference_atoms),
        "natoms": len(reference_atoms),
        keys.time_unit: "fs",
        keys.timestep: get_timestep(time),  # also checks for duplicates
        # "nsteps": len(time) - 1,
        "symbols": reference_atoms.get_chemical_symbols(),
        "masses": reference_atoms.get_masses(),
        keys.reference_atoms: atoms2json(reference_atoms, reduce=False),
        # keys.volume: dataset[keys.volume].data.mean()
    }

    if "primitive" in metadata:
        prim_attrs = {keys.reference_primitive: dict2json(metadata["primitive"])}
        attrs.update(prim_attrs)

    if "supercell" in metadata:
        prim_attrs = {keys.reference_supercell: dict2json(metadata["supercell"])}
        attrs.update(prim_attrs)

    raw_metadata = dict2json(metadata)
    attrs.update({keys.metadata: raw_metadata})

    return attrs
