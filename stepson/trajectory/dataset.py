import xarray as xr
from pathlib import Path
from json import loads
from vibes import keys, dimensions

from vibes.helpers.converters import dict2atoms

from stepson.utils import open_dataset
from .derived_properties import all_properties, add_property


class Trajectory:
    def __init__(self, folder):
        self.data = open_dataset(Path(folder))

        self.atoms_template = loads(self.data.attrs[keys.reference_atoms])
        self.reference_atoms = dict2atoms(self.atoms_template)

    def __len__(self):
        return len(self.data.time)

    def __getattr__(self, key):
        if key in self.data:
            return self.data[key]
        elif key == keys.reference_positions:
            return self.positions_reference
        elif key == keys.reference_lattice:
            return self.lattice_reference
        elif key == "displacements":
            return self.displacements
        elif key == "masses":
            return self.masses
        elif key in all_properties:
            add_property(self.data, key)
            return self.data[key]
        else:
            raise KeyError

    def __getitem__(self, key):
        return getattr(self, key)

    @property
    def masses(self):
        return xr.DataArray(self.reference_atoms.get_masses(), dims=dimensions.I)

    @property
    def positions_reference(self):
        return xr.DataArray(
            self.reference_atoms.get_positions(), dims=dimensions.positions
        )

    @property
    def lattice_reference(self):
        return xr.DataArray(
            self.reference_atoms.get_cell().array, dims=dimensions.lattice
        )

    @property
    def displacements(self):
        reference_positions = self.positions_reference
        displacements = self.positions - reference_positions
        self.data["displacements"] = displacements
        return self.data["displacements"]

    def get_step(self, index):
        return self.data.isel(time=index)

    def get_atoms(self, index):
        return self.step2atoms(self.get_step(index))

    def get_time_and_atoms(self, index):
        return float(self.data.isel(time=index).time.data), self.step2atoms(
            self.get_step(index)
        )

    def step2atoms(self, step):
        atoms_dict = self.atoms_template.copy()

        atoms_dict[keys.velocities] = step[keys.velocities].compute().data
        atoms_dict[keys.positions] = step[keys.positions].compute().data

        if keys.cell in step:
            atoms_dict[keys.cell] = step[keys.cell].compute().data

        return dict2atoms(atoms_dict=atoms_dict)
