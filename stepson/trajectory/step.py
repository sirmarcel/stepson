import numpy as np
from vibes import keys
from ase.constraints import full_3x3_to_voigt_6_stress, voigt_6_to_full_3x3_stress
from ase import units

from stepson.engine import Record
from stepson import comms


class Step(Record):

    meta = "info"
    required = [
        keys.cell,
        keys.positions,
        keys.velocities,
        keys.energy_potential,
        keys.forces,
        "nsteps",
        "dt",
    ]
    optional = [
        keys.stress_potential,
        keys.stresses_potential,
        keys.virials,
        keys.aims_uuid,
        "energies_potential",
        "heat_flux",
        "heat_flux_potential",
        "heat_flux_convective",
    ]

    @classmethod
    def from_entry(cls, dct):
        data, info = extract_data(dct)

        normalize_cell(data)

        if keys.stress in data:
            normalize_stress(data)

        if keys.stresses in data:
            normalize_stresses(data)

        return cls(data, info)


def normalize_cell(data):
    assert data[keys.cell].shape == (3, 3), "unit cell must be 3x3"


def normalize_stress(data):
    if data[keys.stress_potential].shape == (6,):
        data[keys.stress_potential] = voigt_6_to_full_3x3_stress(
            data[keys.stress_potential]
        )

    if np.abs(data[keys.stress_potential]).max() <= 1e-14:
        # zeroed-out stress means no stress was computed
        del data[keys.stress_potential]


def normalize_stresses(data):
    assert keys.stress_potential in data, "stress must be present for stresses"

    stresses = data[keys.stresses_potential]

    if stresses.shape[1:] == (6,):
        stresses = voigt_6_to_full_3x3_stress(stresses)

    stress = data[keys.stress_potential]
    volume = np.abs(np.linalg.det(data[keys.cell]))

    summed_stresses = stresses.sum(axis=0)

    if not np.allclose(stress, summed_stresses):
        summed_stresses /= volume
        diff = np.linalg.norm(stress - summed_stresses)

        if diff > 1e-7:
            msg = f"Stress and stresses differ by {diff}"
            comms.warn(msg, full=True)
            comms.talk(full_3x3_to_voigt_6_stress(stress), full=True)
            comms.talk(full_3x3_to_voigt_6_stress(summed_stresses), full=True)

        stresses /= volume

    data[keys.stresses_potential] = stresses


def extract_data(dct):
    atoms = dct.pop("atoms")
    calc = dct.pop("calculator")
    info = atoms.pop("info")

    data = {}

    # required keys
    for key in [
        keys.cell,
        keys.positions,
        keys.velocities,
    ]:
        data[key] = np.array(atoms.pop(key))

    data[keys.energy_potential] = np.array(calc.pop("energy"))
    data[keys.forces] = np.array(calc.pop(keys.forces))

    data["nsteps"] = info["nsteps"]
    data["dt"] = info.get("dt", units.fs)

    # optional keys with differing names
    if keys.stress in calc:
        data[keys.stress_potential] = calc.pop(keys.stress)

    if "energies" in calc:
        data["energies_potential"] = calc.pop("energies")

    if keys.stresses in calc:
        data[keys.stresses_potential] = calc.pop(keys.stresses)

    # remaining keys
    for key in Step.optional:
        if key in calc:
            data[key] = np.array(calc.pop(key))

    return data, info
