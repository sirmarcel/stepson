from pathlib import Path
import xarray as xr
import numpy as np

from ase.io import read
from vibes import defaults, keys

from stepson import Trajectory

# from stepson.trajectory.derived_properties import getter, add_property
from stepson import comms
from stepson import reader
from stepson.utils import open_dataset


def post(file, outfolder, batchsize, full=False):
    """turn son into dataset"""

    if outfolder is None:
        outfolder = Path(f"trajectory")

    if outfolder.is_dir():
        comms.warn(f"{outfolder} exists, stopping!")
        return None
    else:
        outfolder.mkdir(exist_ok=False)

    reporter = comms.reporter()
    reporter.start(f"post-processing {file}")
    for i, dataset in enumerate(
        reader(
            file,
            full=full,
            batch_size=batchsize,
            properties=[keys.temperature, keys.volume],
        )
    ):
        reporter.step(f"saving batch {i}")
        dataset.to_netcdf(outfolder / f"trajectory_{i:05d}.nc")
        reporter.finish_step()

    reporter.done()


post(Path("./md/trajectory.son"), Path("./md/trajectory/"), 2, full=True)
post(Path("./md/trajectory.son"), Path("./md/trajectory_minimal/"), 2, full=False)

data = Trajectory(Path("./md/trajectory/"))
data_minimal = Trajectory(Path("./md/trajectory_minimal/"))

data_minimal[keys.pressure]
assert keys.pressure in data_minimal.data
np.testing.assert_allclose(data[keys.pressure], data_minimal[keys.pressure])

data_vibes = open_dataset(Path("./md/trajectory.nc"))

print(list(data_vibes.keys()))

for key in data_vibes:
    # print(key)
    np.testing.assert_allclose(data_vibes[key], data[key])

atoms = read("geometry.in.supercell", format="aims")
atoms2 = data.reference_atoms

np.testing.assert_allclose(atoms.get_positions(), atoms2.get_positions())
np.testing.assert_allclose(atoms.get_cell().array, atoms2.get_cell().array)

from stepson.green_kubo import get_heat_flux_dataset, get_kappa_dataset

hf = get_heat_flux_dataset(data.data, include_frequency=False)
k = get_kappa_dataset(hf)
